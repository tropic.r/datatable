let ajax = (url,data={})=>{
    return new Promise((resolve, reject)=>{
        let xhr = new XMLHttpRequest();
        let formData = new FormData();

        if(Object.keys(data).length>0){
            for(let key in data){
                formData.append(key,data[key])
            }
        }

        xhr.open("POST",url)

        xhr.onload = ()=>{
            // console.log(xhr.response)

            resolve(xhr.response)
        }
        xhr.onerror = ()=>{
            console.error('ajax error')

            reject();
        }

        xhr.send(formData)

    })

}

//-------------------------------

let columns = [
    {title:'id'},
    {title:'name'},
    {title:'salary'},
    {title:'position'},
]


function createTable(table,columns) {
    return new Promise((resolve, reject) => {
        ajax('/ajax/example.php',{create:true}).then(response => {
            let json = JSON.parse(response)
            let array = []


            json.forEach(item => {
                let marr = []

                for (let k in item) {
                    marr.push(item[k])

                }
                marr.push(`<a href="#" class="remove" data-id="${item.id}"><i class="fa-solid fa-trash-can"></i></a> 
                           <a href="#" class="edit" data-id="${item.id}"><i class="fa-solid fa-pencil"></i></a> 
                           <a href="#" class="refresh" data-id="${item.id}"><i class="fa-solid fa-arrows-rotate"></i></a>
                         `)
                array.push(marr)
            })

            columns.push({'title': 'редакт.'})


            let t = $(table).DataTable({
                data: array,
                "columns": columns,
                stateSave: true,
                // "processing": true,
                // "serverSide": true,
            });


            resolve()
        })

    })

}





function editable(columns) {
    return new Promise((resolve, reject)=>{
        let tds = document.querySelectorAll(`table tbody td:not(:nth-child(${columns.length}))`)

        tds.forEach(item=>{
            item.setAttribute('contenteditable','true')
        })

        resolve()
    })

}





function remove() {
    const btns = document.querySelectorAll('table td .remove')

    btns.forEach(btn=>{
        btn.addEventListener('click',function () {
            const  id = this.getAttribute('data-id')
            this.parentNode.parentNode.style.opacity='0.5'
            ajax('/ajax/example.php',{remove:true,id}).then(res=>{
                console.log(res)
                this.parentNode.parentNode.remove()
            })

        })
    })
}





function refresh() {
    const btns = document.querySelectorAll('table td .refresh')

    btns.forEach(btn=>{
        btn.addEventListener('click',function () {
            const  id = this.getAttribute('data-id')
            let _this = this
            _this.parentNode.parentNode.style.opacity='0.5'

            ajax('/ajax/example.php',{refresh:true,id}).then(resolve=>{

                let array = JSON.parse(resolve)

                Object.keys(array).forEach((val,key)=>{
                    key+=1
                    let td = _this.parentNode.parentNode.querySelector(`td:nth-child(${key})`)
                    td.innerHTML = array[val]
                })

                _this.parentNode.parentNode.style.opacity='1'

            })

        })
    })
}




function edit(columns) {
    const btns = document.querySelectorAll('table td .edit')
    let obj = {}

    btns.forEach(btn=> {
        btn.addEventListener('click', function () {
            const  id = this.getAttribute('data-id')
            let _this = this
            let tds = _this.parentNode.parentNode.querySelectorAll(`table tbody td:not(:nth-child(${columns.length}))`)
            tds.forEach((td,k)=>{
                obj[columns[k].title] = td.innerHTML
            })
            obj.id = id
            obj.edit = true
            ajax('/ajax/example.php',obj).then(resolve=>{
                console.log(resolve)
            })
        })
    })
}



async function begin() {
    await createTable('#tbl',columns)
    await editable(columns)
    await remove()
    await refresh()
    await edit(columns)
}

begin()



//====================== PART 2


// fetch('/ajax/example.php').then(resp=>{
//    return resp.json();
// }).then(resp=>{
//     console.log(resp)
//
//     $('#tbl').DataTable({
//         data: resp,
//         "columns":[
//             {data:'id'},
//             {data:'id2'},
//             {data:'id3'},
//             {data:'id4'},
//             {data:'id5'},
//             {data:'id6'}
//         ]
//     });
// })


